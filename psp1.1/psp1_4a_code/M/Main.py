#!/usr/bin/python
#-*- coding: utf-8 -*-

import Ipn
import Calculations

class Main:
    def __init__(self):
        self.file_name = ""
        self.data_x = [130, 650, 99, 150, 128, 302, 95, 945, 368, 961]
        self.data_y = [186, 699, 132, 272, 291, 331, 199, 1890, 788, 1601]

    def main(self, ):
        print("File name:")
        self.file_name = raw_input()
        i = Ipn.Ipn(self.file_name)
        self.data = i.read_file()
        for dt in self.data:
            d = map(float, dt.split(" "))
            calc = Calculations.Calculations()
            calc.set_data(d)
            mean = calc.mean()
            std_dev = calc.std_dev()
            print("{0:.2f} {0:.2f}".format(mean, std_dev))
        return
        calc = Calculations.Linear_Regression(self.data_x, self.data_y)
        b1 = calc.calculate_B1()
        cc = calc.calculate_correlation_coefficient()
        b0 = calc.calculate_b0()
        yk = calc.calculate_yk(386)
        print(b1)
        print(cc)
        print(b0)
        print(yk)

if __name__ == "__main__":
    app = Main()
    app.main()

