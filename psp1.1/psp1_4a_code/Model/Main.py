#!/usr/bin/python
#-*- coding: utf-8 -*-

import Ipn
import Calculations
from math import log

class Main:
    def __init__(self):
        self.file_name = ""
        self.data_x = []
        self.data_y = []

    def main(self, ):
        print("File name:")
        self.file_name = raw_input() or "t.txt"
        i = Ipn.Ipn(self.file_name)
        self.data = i.read_file()
        for dt in self.data:
            d = dt.split(",")
            print(d)
            if len(d) is 3:
                loc = float(d[1])/float(d[2])
                ln_loc = log(loc)
                self.data_x.append(ln_loc)
            elif len(d) is 2:
                ln_loc = log(float(d[1]))
                self.data_x.append(ln_loc)
        calc = Calculations.Calculations()
        ss = calc.std_dev(self.data_x)
        log_ranges = Calculations.LogarithmicRanges()

        vs = log_ranges.get_vs(self.data_x)
        s = log_ranges.get_s(self.data_x)
        m = log_ranges.get_m(self.data_x)
        l = log_ranges.get_l(self.data_x)
        vl = log_ranges.get_vl(self.data_x)

        print("{} {} {} {} {}".format(vs, s, m, l, vl))


if __name__ == "__main__":
    app = Main()
    app.main()

