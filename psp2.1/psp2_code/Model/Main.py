#!/usr/bin/python
#-*- coding: utf-8 -*-

import Ipn
import Calculations
from math import log

class Main:
    def __init__(self):
        self.file_name = ""
        self.data_x = []
        self.data_y = []

    def main(self, ):
        self.file_name = input("File name: ") or "t.txt"
        i = Ipn.Ipn(self.file_name)
        self.data = i.read_file()
        sp = Calculations.TDistribution()
        simpson = Calculations.SimpsonIntegration()
        for dt in self.data:
            line = dt.split(" ")
            p = float(line[0])
            dof = float(line[1])

            sp.dof = dof

            simpson.num_seg = 10
            simpson.x_to = 0.99
            simpson.function = sp
            simpson.p = p
            simpson.e = 0.0000001
            simpson.d = 1.0


            result = simpson.calc_p()
            print("{0:.4f}".format(result))

if __name__ == "__main__":
    app = Main()
    app.main()



