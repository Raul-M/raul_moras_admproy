#!/usr/bin/python
#-*- coding: utf-8 -*-

import math
import numpy
class Calculations:
    """
    Class that act as an alternative of the math, have several calculations
    """
    def __init__(self):
        self.data = []
        self.dev_est = None
        self.prom = None

    def set_data(self, data):
        self.data = data

    def get_data(self, ):
        pass

    def sum_all_data(self, data=None):
        if data:
            return sum(data)
        else:
            return sum(self.data)

    def sum_squares(self, data=None):
        if data:
            return sum([x*x for x in data])
        else:
            return sum([x*x for x in self.data])
    
    def sum_x_y(self, data_x, data_y):
        if len(data_x) != len(data_y):
            raise ValueError("Data list must be the same size")
        n = len(data_x)
        return sum([data_x[i] * data_y[i] for i in range(n)])


    def mean(self, data=None):
        if data:
            n = len(data)
            if n < 1:
                raise ValueError('At least one number is required')
            return sum(data)/n
        else:
            n = len(self.data)
            if n < 1:
                raise ValueError('At least one number is required')
            return sum(self.data)/n

    def ss(self, data=None):
        if data:
            n = len(data)
            c = self.mean(data)
            ss = sum((x-c)**2 for x in data)
        else:
            n = len(self.data)
            c = self.mean()
            ss = sum((x-c)**2 for x in self.data)
        pvar = ss/(n-1)
        return pvar

    def std_dev(self, data=None):
        if data:
            n = len(data)
            if n < 2:
                raise ValueError("At least two numbers are required")
            ss = self.ss(data)
        else:
            n = len(self.data)
            if n < 2:
                raise ValueError("At least two numbers are required")
            ss = self.ss()
        return ss ** 0.5


class Linear_Regression:
    """
    Class that uses the class Calculations to obtain the linear regression of
    two lists
    """
    def __init__(self, data_x, data_y):
        self.data_x = data_x
        self.data_y = data_y

    def calculate_B1(self):
        calc = Calculations()
        n     = len(self.data_x)
        x_avg = calc.mean(self.data_x)
        y_avg = calc.mean(self.data_y)
        x_sum = calc.sum_all_data(self.data_x)
        y_sum = calc.sum_all_data(self.data_y)
        x_y   = calc.sum_x_y(self.data_x, self.data_y)
        x_pow = calc.sum_squares(self.data_x)

        b1 = ( (x_y - n * x_avg * y_avg) / (x_pow - n * x_avg**2) ) 

        return b1
    def calculate_correlation_coefficient(self):
        from math import sqrt
        calc = Calculations()
        n     = len(self.data_x)
        x_sum = calc.sum_all_data(self.data_x)
        y_sum = calc.sum_all_data(self.data_y)
        x_y   = calc.sum_x_y(self.data_x, self.data_y)
        x_pow = calc.sum_squares(self.data_x)
        y_pow = calc.sum_squares(self.data_y)

        cc_up = n * x_y - (x_sum * y_sum)
        cc_down = sqrt( (n*x_pow - x_sum**2) * (n*y_pow - y_sum**2) )

        cc =  cc_up/cc_down
        return cc

    def calculate_b0(self):
        calc = Calculations()
        y_avg = calc.mean(self.data_y)
        x_avg = calc.mean(self.data_x)
        b1 = self.calculate_B1()

        b0 = y_avg - b1*x_avg
        return b0

    def calculate_yk(self, xk):
        b1 = self.calculate_B1()
        b0 = self.calculate_b0()

        yk = b0 + b1*xk

        return yk

class LogarithmicRanges:
    def __init__(self):
        self.data = []

    def get_vs(self, data):
        calc = Calculations()
        std_dev = calc.std_dev(data)
        mean = calc.mean(data)
        ln_vs = mean - 2*std_dev 

        return math.exp(ln_vs)
     
    def get_s(self, data):
        calc = Calculations()
        std_dev = calc.std_dev(data)
        mean = calc.mean(data)
        ln_s = mean - std_dev 

        return math.exp(ln_s)

    def get_m(self, data):
        calc = Calculations()
        std_dev = calc.std_dev(data)
        mean = calc.mean(data)
        ln_m = mean 

        return math.exp(ln_m)

    def get_l(self, data):
        calc = Calculations()
        std_dev = calc.std_dev(data)
        mean = calc.mean(data)
        ln_l = mean + std_dev 

        return math.exp(ln_l)

    def get_vl(self, data):
        calc = Calculations()
        std_dev = calc.std_dev(data)
        mean = calc.mean(data)
        ln_vl = mean + 2*std_dev 

        return math.exp(ln_vl)


class SimpsonIntegration:

    def __init__(self):
        self.data = []
        self.num_seg = 0
        self.w = 0
        self.x_from = 0.0
        self.x_to = 0.0 
        self.error = 0.00001
        self.function = None
        self.p = 0
        self.d = 0.5

    def calc_result(self, num_seg=None, x_to=None, e=None, function=None):
        if num_seg is None:
            num_seg = self.num_seg
        if x_to is None:
            x_to = self.x_to
        if e is None:
            e = self.error
        if function is None:
            function = self.function
        w = float(x_to)/float(num_seg)
        result = function.calc_result(x=0) * w/3
        xi = numpy.arange(w, x_to,w)
        for i,x in enumerate(xi):
            if i % 2 == 0:
                r = function.calc_result(x=x)
                r = r * 4 * w/3
                result += r
            else:
                r = function.calc_result(x=x)
                r = r * 2 * w/3
                result += r
        result += function.calc_result(x=x_to) * w/3

        self.result = result
        return self.result


    def calc_p(self):
        last = "pos"
        sign = "pos"
        dif = 10.0

        result = 0
        while(abs(dif) > self.e):
            result = self.calc_result()
            if result < self.p:
                self.x_to += self.d
            else:
                self.x_to -= self.d
            if dif <= 0:
                sign = "neg"
            else:
                sign = "pos"
            if sign != last:
                self.d /= 2
            last = sign
            dif = result - self.p
            if self.d < self.e:
                return self.x_to
        return self.x_to

    def calc_pi(self, num_seg=None, x_to=None, e=None, function=None, p=None, d=None):
        if num_seg is None:
            num_seg = self.num_seg
        if p is None:
            p = self.p
        if x_to is None:
            x_to = 1.0
        if e is None:
            e = 0.00001
        if d is None:
            d = 0.5
        if function is None:
            function = self.function

        dif = 1.0
        last = "pos"
        sign = "pos"
        while(abs(dif) > e):
            result = self.calc_result()
            dif = result - p
            if dif < 0:
                sign = "neg"
            else:
                sign = "pos"
            if last != sign:
                d/=2
            last = sign
            if result < p:
                self.x_to += d
            elif result > p:
                self.x_to -= d
            if d <= e:
                return self.x_to
        
        return self.x_to



class TDistribution:

    def __init__(self):
        self.dof = 2
        self.result = 0
        self.x = 1

    def gamma_function(self, x):
        if x == 1:
            return 1
        elif x == 1/2:
            return pow(math.pi, 1/2)
        else:
            return (x-1) * self.gamma_function(x-1)

    def calc_result(self, x=None, dof=None):
        if dof is None:
            dof = self.dof
        if x is None:
            x = self.x
        dividendo = self.gamma_function((dof + 1)/2)
        divisor_p1 = pow(dof * math.pi, 1/2)
        divisor_p2 = self.gamma_function(dof/2) 
        divisor = pow(dof * math.pi, 1/2) * self.gamma_function(dof/2)
        div = dividendo/divisor
        mult = pow((1+(pow(x,2)/dof)), -(dof+1)/2)

        # print("Div1 " + str(divisor_p1))
        # print("Div2 " + str(divisor_p2))
        # print("Div " + str(div))
        # print("Mult " + str(mult))

        self.result = dividendo/divisor * mult
        # print("Result " + str(self.result))
        return self.result
