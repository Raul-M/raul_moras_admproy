#!/usr/bin/python
#-*- coding: utf-8 -*-

import Ipn
import Calculations
import numpy as np
from math import log
from math import sqrt

class Main:
    def __init__(self):
        self.file_name = ""
        self.data_x = []
        self.data_y = []

    def main(self, ):
        self.file_name = input("File name: ") or "t.txt"
        i = Ipn.Ipn(self.file_name)
        self.data = i.read_file()
        sp = Calculations.TDistribution()
        simpson = Calculations.SimpsonIntegration()
        calc = Calculations.Calculations()
        wk = 650
        xk = 3000
        yk = 155
        w = []
        x = []
        y = []
        z = []
        p = 0.35
        dof = 2
        sp.dof = dof
        simpson.num_seg = 10
        simpson.x_to = 0.99
        simpson.function = sp
        simpson.p = p
        simpson.e = 0.0000001
        simpson.d = 1.0
        result = simpson.calc_p()
        for dt in self.data:
            line = dt.split(" ")
            w.append(int(line[0]))
            x.append(int(line[1]))
            y.append(int(line[2]))
            z.append(int(line[3]))

        n = len(self.data)
        sum_w = sum(w)
        sum_x = sum(x)
        sum_y = sum(y)
        sum_z = sum(z)

        w2 = calc.sum_squares(w)
        wx = calc.sum_x_y(w,x)
        wy = calc.sum_x_y(w,y)
        wz = calc.sum_x_y(w,z)

        x2 = calc.sum_squares(x)
        xy = calc.sum_x_y(x,y)
        xz = calc.sum_x_y(x,z)

        y2 = calc.sum_squares(y)
        yz = calc.sum_x_y(y,z)

        results = [sum_z, wz, xz, yz]
        p1 = [n, sum_w, sum_x, sum_y]
        p2 = [sum_w, w2, wx, wy]
        p3 = [sum_x, wx, x2, xy]
        p4 = [sum_y, wy, xy, y2]

        A = np.array([p1, p2, p3, p4])
        B = np.array(results)
        Betas = np.linalg.solve(A,B)
        print("B0 {} - B1 {} - B2 {} - B3 {}".format(Betas[0], Betas[1], Betas[2], Betas[3]))

        #variance
        sum_variance = 0
        for i in range(n):
            sum_variance += ((z[i] - Betas[0] - (Betas[1] * w[i]) - (Betas[2] * x[i]) - (Betas[3] * y[i])) ** 2)
        variance = (1 / (n - 4)) * sum_variance

        #standard deviation
        std_dev = sqrt(variance)

        #range
        res_range = 0
        wavg = sum_w / n
        xavg = sum_x / n
        yavg = sum_y / n
        sumatory_w = 0
        sumatory_x = 0
        sumatory_y = 0
        for i in range(n):
            sumatory_w += (w[i] - wavg) ** 2
            sumatory_x += (x[i] - xavg) ** 2
            sumatory_y += (y[i] - yavg) ** 2
        res_range = 1 + (1 / n) + ((wk - wavg) ** 2 / sumatory_w) + ((xk - xavg) ** 2 / sumatory_x) + ((yk - yavg) ** 2 / sumatory_y)
        ranges = result * std_dev * sqrt(res_range)
        print("Prediction range: {}".format(ranges))

        #estimated time
        z_res = Betas[0] + (Betas[1] * wk) + (Betas[2] * xk) + (Betas[3] * yk)
        print ("Estimated time: {}".format(z_res))


if __name__ == "__main__":
    app = Main()
    app.main()



