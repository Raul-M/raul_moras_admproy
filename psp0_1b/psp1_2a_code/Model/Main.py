#!/usr/bin/python
#-*- coding: utf-8 -*-

import Counter
import Handle_Files
import Usage
import getopt
import sys


class Main:

	def __init__(self):
		self.files = []
		self.total_lines = 0

	def main(self):
	    try:
	        myopt, args = getopt.getopt(sys.argv[1:], "[h:f]", ["files"])
	    except getopt.GetoptError:
	    	use = Usage.Usage()
	        use.print_usage()

	    # If there are no files names
	    if not len(args) or not len(myopt):
	        use = Usage.Usage()
	        use.print_usage()

	    # Check the options, if it is -h shows help, if it -f it runs
	    # the program else prints the usage
	    for opt in myopt:
	        if opt[0] in '-h':
	            usage()
	        elif opt[0] in '-f':
	        	hf = Handle_Files.Handle_Files()
	        	hf.set_files(args)
	        	total_lines = hf.handle_files()
	        	print("Total number of lines: {}".format(total_lines))
	        else:
	            usage()

if __name__ == "__main__":
    app = Main()
    app.main()
