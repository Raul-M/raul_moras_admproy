#!/usr/bin/python
#-*- coding: utf-8 -*-

class Counter:
    """
    Clase que cuenta lineas
    """
    def __init__(self):
        self.count = 0
        self.unsupported = {"#", "import"}
        self.blank_lines = {'\n', '\r\n'}
        self.is_function = "def"
        self.lines = None
        self.functions = dict()
        self.counter_function = 0
        self.in_function = 0
        self.actual_function = ""

    def count_lines(self, ):
    	for line in self.lines:
    		try:
                    if line.split()[0] == "def":
                        self.functions[self.actual_function] = self.counter_function
                        self.counter_function = 0
                        self.actual_function = line.split()[1]
                    if line in self.blank_lines:
                        pass
                    elif line.split()[0] in self.unsupported:
                        pass
                    else:
                        self.counter_function += 1
                        self.count += 1
    		except Exception as e:
    			pass
        	

        self.count += 1    	
        self.functions.pop('', None)
        self.functions[self.actual_function] = self.counter_function
        for key, item in self.functions.items():
            print("{} {}".format(key.rstrip(":"), item))
        return self.count
        

    def set_lines(self, lines):
    	self.lines = lines
        return

    def get_lines(self, ):
        return self.lines
