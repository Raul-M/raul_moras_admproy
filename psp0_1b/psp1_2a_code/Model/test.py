import unittest
import main

class testMain(unittest.TestCase):
    def runTest(self):
        try:
            with open("main.py", "r") as data:
                lines = data.readlines()

                nLines = main.countLines(lines)
                assert nLines == 29, "Incorrect numbers of lines"
        except IOError:
            print("No file name")
            return

class testMainA1(unittest.TestCase):
    def runTest(self):
        try:
            with open("../A1/main.py", "r") as data:
                lines = data.readlines()

                nLines = main.countLines(lines)
                assert nLines == 36, "Incorrect numbers of lines"
        except IOError:
            print("No file name")
            return

class testTestSD(unittest.TestCase):
    def runTest(self):
        try:
            with open("../A1/testSD.py", "r") as data:
                lines = data.readlines()

                nLines = main.countLines(lines)
                assert nLines == 26, "Incorrect numbers of lines"
        except IOError:
            print("No file name")
            return

if __name__ == '__main__':
    unittest.main()
