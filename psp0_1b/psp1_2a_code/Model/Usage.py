#!/usr/bin/python
#-*- coding: utf-8 -*-
import sys


class Usage:
    def __init__(self):
        self.usage = (
        "\nThis program will count the number of lines of code in the file,"
        "without counting the whitespaces, the line comments and the imports"
    )

    def print_usage(self, ):
        print self.usage
        sys.exit(2)

