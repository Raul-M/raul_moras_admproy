'''This program will count the number of lines of code in the file,
without counting the whitespaces, the line comments and the imports.

Raul Moras
python main.py (args)
'''

def main():
    '''This is the main function where all the lines are counted'''

    name_file = input()

    # Try to open the file name
    try:
        with open(name_file, 'r') as data:
            lines = data.readlines()
            nLines = countLines(lines)

            print("Number of lines: {}".format(nLines))
    except IOError:
        print("No file name")


def countLines(lines):
    '''This function is the one in charged of counting the lines
    int the file
    '''

    count = 0

    unsupported = {"#", "import"}
    for line in lines:
        if line in ('\n', '\r\n'):
            pass
        elif line.split()[0] in unsupported:
            pass
        else:
            count += 1

    return count

# main()
