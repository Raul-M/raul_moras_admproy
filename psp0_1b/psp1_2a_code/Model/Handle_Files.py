#!/usr/bin/python
#-*- coding: utf-8 -*-

import Counter


class Handle_Files:
    def __init__(self):
        self.total_lines = None
        self.files = None

    def get_lines(self, ):
        return self.files

    def set_files(self, files):
    	self.files = files
        return

    def handle_files(self):
    	total_lines = 0
    	for f_name in self.files:
        	name_file = f_name

        	# Try to open the file name
        	try:
        		with open(name_file, 'r') as data:
					lines = data.readlines()
					counter = Counter.Counter()
					counter.set_lines(lines)
					num_lines = counter.count_lines()
					total_lines += num_lines
					print("Number of lines in {}: {}".format(f_name, num_lines))
					data.close()
        	except UnicodeDecodeError:
				print("No valid file {}".format(f_name))
        	except IOError:
				print("No file name {}".format(f_name))
        self.total_lines = total_lines    	
    	return total_lines