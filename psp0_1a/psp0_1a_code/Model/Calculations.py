#!/usr/bin/python
#-*- coding: utf-8 -*-

class Calculations:
    def __init__(self):
        self.data = []
        self.dev_est = None
        self.prom = None

    def set_data(self, data):
        self.data = data

    def get_data(self, ):
        pass

    def mean(self):
        n = len(self.data)
        if n < 1:
            raise ValueError('At least one number is required')
        return sum(self.data)/n

    def _ss(self):
        c = self.mean()
        ss = sum((x-c)**2 for x in self.data)
        return ss

    def std_dev(self):
        n = len(self.data)
        if n < 2:
            raise ValueError("At least two numbers are required")
        ss = self._ss()
        pvar = ss/(n-1)
        return pvar ** 0.5

