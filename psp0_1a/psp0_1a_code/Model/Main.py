#!/usr/bin/python
#-*- coding: utf-8 -*-

import Ipn
import Calculations

class Main:
    def __init__(self):
        self.file_name = ""
        self.data = []

    def main(self, ):
        print("File name:")
        self.file_name = raw_input()
        i = Ipn.Ipn(self.file_name)
        self.data = i.read_file()
        for dt in self.data:
            d = map(float, dt.split(" "))
            calc = Calculations.Calculations()
            calc.set_data(d)
            mean = calc.mean()
            std_dev = calc.std_dev()
            print("{0:.2f} {0:.2f}".format(mean, std_dev))
        return

if __name__ == "__main__":
    app = Main()
    app.main()

