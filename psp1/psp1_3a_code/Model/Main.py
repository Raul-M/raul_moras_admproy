#!/usr/bin/python
#-*- coding: utf-8 -*-

import Ipn
import Calculations

class Main:
    def __init__(self):
        self.file_name = ""
        self.data_x = []
        self.data_y = []

    def main(self, ):
        print("File name:")
        self.file_name = raw_input() or "t.txt"
        i = Ipn.Ipn(self.file_name)
        self.data = i.read_file()
        for dt in self.data:
            d = map(float, dt.split(" "))
            self.data_x.append(d[0])
            self.data_y.append(d[1])
            calc = Calculations.Calculations()
        calc = Calculations.Linear_Regression(self.data_x, self.data_y)
        b1 = calc.calculate_B1()
        cc = calc.calculate_correlation_coefficient()
        cc_pow = cc**2
        b0 = calc.calculate_b0()
        yk = calc.calculate_yk(386)
        print(b0)
        print(b1)
        print(cc)
        print(cc_pow)
        print(yk)

if __name__ == "__main__":
    app = Main()
    app.main()

