#!/usr/bin/python
#-*- coding: utf-8 -*-

class Calculations:
    """
    Class that act as an alternative of the math, have several calculations
    """
    def __init__(self):
        self.data = []
        self.dev_est = None
        self.prom = None

    def set_data(self, data):
        self.data = data

    def get_data(self, ):
        pass

    def sum_all_data(self, data=None):
        if data:
            return sum(data)
        else:
            return sum(self.data)

    def sum_squares(self, data=None):
        if data:
            return sum([x*x for x in data])
        else:
            return sum([x*x for x in self.data])
    
    def sum_x_y(self, data_x, data_y):
        if len(data_x) != len(data_y):
            raise ValueError("Data list must be the same size")
        n = len(data_x)
        return sum([data_x[i] * data_y[i] for i in range(n)])


    def mean(self, data=None):
        if data:
            n = len(data)
            if n < 1:
                raise ValueError('At least one number is required')
            return sum(data)/n
        else:
            n = len(self.data)
            if n < 1:
                raise ValueError('At least one number is required')
            return sum(self.data)/n

    def _ss(self):
        c = self.mean()
        ss = sum((x-c)**2 for x in self.data)
        return ss

    def std_dev(self):
        n = len(self.data)
        if n < 2:
            raise ValueError("At least two numbers are required")
        ss = self._ss()
        pvar = ss/(n-1)
        return pvar ** 0.5


class Linear_Regression:
    """
    Class that uses the class Calculations to obtain the linear regression of
    two lists
    """
    def __init__(self, data_x, data_y):
        self.data_x = data_x
        self.data_y = data_y

    def calculate_B1(self):
        calc = Calculations()
        n     = len(self.data_x)
        x_avg = calc.mean(self.data_x)
        y_avg = calc.mean(self.data_y)
        x_sum = calc.sum_all_data(self.data_x)
        y_sum = calc.sum_all_data(self.data_y)
        x_y   = calc.sum_x_y(self.data_x, self.data_y)
        x_pow = calc.sum_squares(self.data_x)

        b1 = ( (x_y - n * x_avg * y_avg) / (x_pow - n * x_avg**2) ) 

        return b1
    def calculate_correlation_coefficient(self):
        from math import sqrt
        calc = Calculations()
        n     = len(self.data_x)
        x_sum = calc.sum_all_data(self.data_x)
        y_sum = calc.sum_all_data(self.data_y)
        x_y   = calc.sum_x_y(self.data_x, self.data_y)
        x_pow = calc.sum_squares(self.data_x)
        y_pow = calc.sum_squares(self.data_y)

        cc_up = n * x_y - (x_sum * y_sum)
        cc_down = sqrt( (n*x_pow - x_sum**2) * (n*y_pow - y_sum**2) )

        cc =  cc_up/cc_down
        return cc

    def calculate_b0(self):
        calc = Calculations()
        y_avg = calc.mean(self.data_y)
        x_avg = calc.mean(self.data_x)
        b1 = self.calculate_B1()

        b0 = y_avg - b1*x_avg
        return b0

    def calculate_yk(self, xk):
        b1 = self.calculate_B1()
        b0 = self.calculate_b0()

        yk = b0 + b1*xk

        return yk


